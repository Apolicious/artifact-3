// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "McCoskeyFPS/Camera_Director.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCamera_Director() {}
// Cross Module References
	MCCOSKEYFPS_API UClass* Z_Construct_UClass_ACamera_Director_NoRegister();
	MCCOSKEYFPS_API UClass* Z_Construct_UClass_ACamera_Director();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_McCoskeyFPS();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void ACamera_Director::StaticRegisterNativesACamera_Director()
	{
	}
	UClass* Z_Construct_UClass_ACamera_Director_NoRegister()
	{
		return ACamera_Director::StaticClass();
	}
	struct Z_Construct_UClass_ACamera_Director_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraTwo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraTwo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraOne_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraOne;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACamera_Director_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_McCoskeyFPS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACamera_Director_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Camera_Director.h" },
		{ "ModuleRelativePath", "Camera_Director.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraTwo_MetaData[] = {
		{ "Category", "Camera_Director" },
		{ "ModuleRelativePath", "Camera_Director.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraTwo = { "CameraTwo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACamera_Director, CameraTwo), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraTwo_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraTwo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraOne_MetaData[] = {
		{ "Category", "Camera_Director" },
		{ "ModuleRelativePath", "Camera_Director.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraOne = { "CameraOne", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACamera_Director, CameraOne), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraOne_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraOne_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACamera_Director_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraTwo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACamera_Director_Statics::NewProp_CameraOne,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACamera_Director_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACamera_Director>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACamera_Director_Statics::ClassParams = {
		&ACamera_Director::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ACamera_Director_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ACamera_Director_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ACamera_Director_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACamera_Director_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACamera_Director()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACamera_Director_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACamera_Director, 333312655);
	template<> MCCOSKEYFPS_API UClass* StaticClass<ACamera_Director>()
	{
		return ACamera_Director::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACamera_Director(Z_Construct_UClass_ACamera_Director, &ACamera_Director::StaticClass, TEXT("/Script/McCoskeyFPS"), TEXT("ACamera_Director"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACamera_Director);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
