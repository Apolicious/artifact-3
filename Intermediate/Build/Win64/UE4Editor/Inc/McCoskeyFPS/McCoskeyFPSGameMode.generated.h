// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGamePlayState : int32;
#ifdef MCCOSKEYFPS_McCoskeyFPSGameMode_generated_h
#error "McCoskeyFPSGameMode.generated.h already included, missing '#pragma once' in McCoskeyFPSGameMode.h"
#endif
#define MCCOSKEYFPS_McCoskeyFPSGameMode_generated_h

#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMcCoskeyFPSGameMode(); \
	friend struct Z_Construct_UClass_AMcCoskeyFPSGameMode_Statics; \
public: \
	DECLARE_CLASS(AMcCoskeyFPSGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), MCCOSKEYFPS_API) \
	DECLARE_SERIALIZER(AMcCoskeyFPSGameMode)


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAMcCoskeyFPSGameMode(); \
	friend struct Z_Construct_UClass_AMcCoskeyFPSGameMode_Statics; \
public: \
	DECLARE_CLASS(AMcCoskeyFPSGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), MCCOSKEYFPS_API) \
	DECLARE_SERIALIZER(AMcCoskeyFPSGameMode)


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MCCOSKEYFPS_API AMcCoskeyFPSGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMcCoskeyFPSGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MCCOSKEYFPS_API, AMcCoskeyFPSGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMcCoskeyFPSGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MCCOSKEYFPS_API AMcCoskeyFPSGameMode(AMcCoskeyFPSGameMode&&); \
	MCCOSKEYFPS_API AMcCoskeyFPSGameMode(const AMcCoskeyFPSGameMode&); \
public:


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MCCOSKEYFPS_API AMcCoskeyFPSGameMode(AMcCoskeyFPSGameMode&&); \
	MCCOSKEYFPS_API AMcCoskeyFPSGameMode(const AMcCoskeyFPSGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MCCOSKEYFPS_API, AMcCoskeyFPSGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMcCoskeyFPSGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMcCoskeyFPSGameMode)


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_PRIVATE_PROPERTY_OFFSET
#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_19_PROLOG
#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_RPC_WRAPPERS \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_INCLASS \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_INCLASS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MCCOSKEYFPS_API UClass* StaticClass<class AMcCoskeyFPSGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact_3_Source_McCoskeyFPS_McCoskeyFPSGameMode_h


#define FOREACH_ENUM_EGAMEPLAYSTATE(op) \
	op(EGamePlayState::EPlaying) \
	op(EGamePlayState::EGameOver) \
	op(EGamePlayState::EUnknown) 

enum class EGamePlayState;
template<> MCCOSKEYFPS_API UEnum* StaticEnum<EGamePlayState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
