// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATargetPoint;
#ifdef MCCOSKEYFPS_TagController_generated_h
#error "TagController.generated.h already included, missing '#pragma once' in TagController.h"
#endif
#define MCCOSKEYFPS_TagController_generated_h

#define artifact_3_Source_McCoskeyFPS_TagController_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGoToRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoToRandomWaypoint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ATargetPoint**)Z_Param__Result=P_THIS->GetRandomWaypoint(); \
		P_NATIVE_END; \
	}


#define artifact_3_Source_McCoskeyFPS_TagController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGoToRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoToRandomWaypoint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ATargetPoint**)Z_Param__Result=P_THIS->GetRandomWaypoint(); \
		P_NATIVE_END; \
	}


#define artifact_3_Source_McCoskeyFPS_TagController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATagController(); \
	friend struct Z_Construct_UClass_ATagController_Statics; \
public: \
	DECLARE_CLASS(ATagController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(ATagController)


#define artifact_3_Source_McCoskeyFPS_TagController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesATagController(); \
	friend struct Z_Construct_UClass_ATagController_Statics; \
public: \
	DECLARE_CLASS(ATagController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(ATagController)


#define artifact_3_Source_McCoskeyFPS_TagController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATagController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATagController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATagController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATagController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATagController(ATagController&&); \
	NO_API ATagController(const ATagController&); \
public:


#define artifact_3_Source_McCoskeyFPS_TagController_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATagController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATagController(ATagController&&); \
	NO_API ATagController(const ATagController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATagController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATagController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATagController)


#define artifact_3_Source_McCoskeyFPS_TagController_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Waypoints() { return STRUCT_OFFSET(ATagController, Waypoints); }


#define artifact_3_Source_McCoskeyFPS_TagController_h_13_PROLOG
#define artifact_3_Source_McCoskeyFPS_TagController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_TagController_h_16_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_TagController_h_16_RPC_WRAPPERS \
	artifact_3_Source_McCoskeyFPS_TagController_h_16_INCLASS \
	artifact_3_Source_McCoskeyFPS_TagController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact_3_Source_McCoskeyFPS_TagController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_TagController_h_16_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_TagController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_TagController_h_16_INCLASS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_TagController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MCCOSKEYFPS_API UClass* StaticClass<class ATagController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact_3_Source_McCoskeyFPS_TagController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
