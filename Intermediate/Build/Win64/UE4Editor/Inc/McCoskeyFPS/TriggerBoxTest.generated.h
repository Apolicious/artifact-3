// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef MCCOSKEYFPS_TriggerBoxTest_generated_h
#error "TriggerBoxTest.generated.h already included, missing '#pragma once' in TriggerBoxTest.h"
#endif
#define MCCOSKEYFPS_TriggerBoxTest_generated_h

#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execToggleLight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleLight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	}


#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleLight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleLight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	}


#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATriggerBoxTest(); \
	friend struct Z_Construct_UClass_ATriggerBoxTest_Statics; \
public: \
	DECLARE_CLASS(ATriggerBoxTest, ATriggerBox, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(ATriggerBoxTest)


#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_INCLASS \
private: \
	static void StaticRegisterNativesATriggerBoxTest(); \
	friend struct Z_Construct_UClass_ATriggerBoxTest_Statics; \
public: \
	DECLARE_CLASS(ATriggerBoxTest, ATriggerBox, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(ATriggerBoxTest)


#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATriggerBoxTest(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATriggerBoxTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATriggerBoxTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATriggerBoxTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATriggerBoxTest(ATriggerBoxTest&&); \
	NO_API ATriggerBoxTest(const ATriggerBoxTest&); \
public:


#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATriggerBoxTest(ATriggerBoxTest&&); \
	NO_API ATriggerBoxTest(const ATriggerBoxTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATriggerBoxTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATriggerBoxTest); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATriggerBoxTest)


#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_PRIVATE_PROPERTY_OFFSET
#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_13_PROLOG
#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_RPC_WRAPPERS \
	artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_INCLASS \
	artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_INCLASS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MCCOSKEYFPS_API UClass* StaticClass<class ATriggerBoxTest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact_3_Source_McCoskeyFPS_TriggerBoxTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
