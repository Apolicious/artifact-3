// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MCCOSKEYFPS_McCoskeyFPSHUD_generated_h
#error "McCoskeyFPSHUD.generated.h already included, missing '#pragma once' in McCoskeyFPSHUD.h"
#endif
#define MCCOSKEYFPS_McCoskeyFPSHUD_generated_h

#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_RPC_WRAPPERS
#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMcCoskeyFPSHUD(); \
	friend struct Z_Construct_UClass_AMcCoskeyFPSHUD_Statics; \
public: \
	DECLARE_CLASS(AMcCoskeyFPSHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(AMcCoskeyFPSHUD)


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMcCoskeyFPSHUD(); \
	friend struct Z_Construct_UClass_AMcCoskeyFPSHUD_Statics; \
public: \
	DECLARE_CLASS(AMcCoskeyFPSHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(AMcCoskeyFPSHUD)


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMcCoskeyFPSHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMcCoskeyFPSHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMcCoskeyFPSHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMcCoskeyFPSHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMcCoskeyFPSHUD(AMcCoskeyFPSHUD&&); \
	NO_API AMcCoskeyFPSHUD(const AMcCoskeyFPSHUD&); \
public:


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMcCoskeyFPSHUD(AMcCoskeyFPSHUD&&); \
	NO_API AMcCoskeyFPSHUD(const AMcCoskeyFPSHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMcCoskeyFPSHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMcCoskeyFPSHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMcCoskeyFPSHUD)


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HUDWidgetClass() { return STRUCT_OFFSET(AMcCoskeyFPSHUD, HUDWidgetClass); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(AMcCoskeyFPSHUD, CurrentWidget); }


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_9_PROLOG
#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_RPC_WRAPPERS \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_INCLASS \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_INCLASS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MCCOSKEYFPS_API UClass* StaticClass<class AMcCoskeyFPSHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact_3_Source_McCoskeyFPS_McCoskeyFPSHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
