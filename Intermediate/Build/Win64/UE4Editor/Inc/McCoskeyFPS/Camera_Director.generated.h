// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MCCOSKEYFPS_Camera_Director_generated_h
#error "Camera_Director.generated.h already included, missing '#pragma once' in Camera_Director.h"
#endif
#define MCCOSKEYFPS_Camera_Director_generated_h

#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_RPC_WRAPPERS
#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACamera_Director(); \
	friend struct Z_Construct_UClass_ACamera_Director_Statics; \
public: \
	DECLARE_CLASS(ACamera_Director, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(ACamera_Director)


#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACamera_Director(); \
	friend struct Z_Construct_UClass_ACamera_Director_Statics; \
public: \
	DECLARE_CLASS(ACamera_Director, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(ACamera_Director)


#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACamera_Director(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACamera_Director) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACamera_Director); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACamera_Director); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACamera_Director(ACamera_Director&&); \
	NO_API ACamera_Director(const ACamera_Director&); \
public:


#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACamera_Director(ACamera_Director&&); \
	NO_API ACamera_Director(const ACamera_Director&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACamera_Director); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACamera_Director); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACamera_Director)


#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_PRIVATE_PROPERTY_OFFSET
#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_9_PROLOG
#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_RPC_WRAPPERS \
	artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_INCLASS \
	artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_INCLASS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_Camera_Director_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MCCOSKEYFPS_API UClass* StaticClass<class ACamera_Director>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact_3_Source_McCoskeyFPS_Camera_Director_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
