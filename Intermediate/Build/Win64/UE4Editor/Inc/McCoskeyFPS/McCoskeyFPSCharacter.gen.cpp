// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "McCoskeyFPS/McCoskeyFPSCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMcCoskeyFPSCharacter() {}
// Cross Module References
	MCCOSKEYFPS_API UClass* Z_Construct_UClass_AMcCoskeyFPSCharacter_NoRegister();
	MCCOSKEYFPS_API UClass* Z_Construct_UClass_AMcCoskeyFPSCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_McCoskeyFPS();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth();
	MCCOSKEYFPS_API UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCurveFloat_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAnimMontage_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USoundBase_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	MCCOSKEYFPS_API UClass* Z_Construct_UClass_AMcCoskeyFPSProjectile_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	HEADMOUNTEDDISPLAY_API UClass* Z_Construct_UClass_UMotionControllerComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
// End Cross Module References
	void AMcCoskeyFPSCharacter::StaticRegisterNativesAMcCoskeyFPSCharacter()
	{
		UClass* Class = AMcCoskeyFPSCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DamageTimer", &AMcCoskeyFPSCharacter::execDamageTimer },
			{ "GetHealth", &AMcCoskeyFPSCharacter::execGetHealth },
			{ "GetHealthIntText", &AMcCoskeyFPSCharacter::execGetHealthIntText },
			{ "GetMagic", &AMcCoskeyFPSCharacter::execGetMagic },
			{ "GetMagicIntText", &AMcCoskeyFPSCharacter::execGetMagicIntText },
			{ "PlayFlash", &AMcCoskeyFPSCharacter::execPlayFlash },
			{ "SetDamageState", &AMcCoskeyFPSCharacter::execSetDamageState },
			{ "SetMagicChange", &AMcCoskeyFPSCharacter::execSetMagicChange },
			{ "SetMagicState", &AMcCoskeyFPSCharacter::execSetMagicState },
			{ "SetMagicValue", &AMcCoskeyFPSCharacter::execSetMagicValue },
			{ "UpdateHealth", &AMcCoskeyFPSCharacter::execUpdateHealth },
			{ "UpdateMagic", &AMcCoskeyFPSCharacter::execUpdateMagic },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Damage Timer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "DamageTimer", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics
	{
		struct McCoskeyFPSCharacter_eventGetHealth_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(McCoskeyFPSCharacter_eventGetHealth_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Get Health" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "GetHealth", sizeof(McCoskeyFPSCharacter_eventGetHealth_Parms), Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics
	{
		struct McCoskeyFPSCharacter_eventGetHealthIntText_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(McCoskeyFPSCharacter_eventGetHealthIntText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Get Health Text" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "GetHealthIntText", sizeof(McCoskeyFPSCharacter_eventGetHealthIntText_Parms), Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics
	{
		struct McCoskeyFPSCharacter_eventGetMagic_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(McCoskeyFPSCharacter_eventGetMagic_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::Function_MetaDataParams[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Get Magic" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "GetMagic", sizeof(McCoskeyFPSCharacter_eventGetMagic_Parms), Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics
	{
		struct McCoskeyFPSCharacter_eventGetMagicIntText_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(McCoskeyFPSCharacter_eventGetMagicIntText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::Function_MetaDataParams[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Get Magic Text" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "GetMagicIntText", sizeof(McCoskeyFPSCharacter_eventGetMagicIntText_Parms), Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics
	{
		struct McCoskeyFPSCharacter_eventPlayFlash_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((McCoskeyFPSCharacter_eventPlayFlash_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(McCoskeyFPSCharacter_eventPlayFlash_Parms), &Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Play Flash" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "PlayFlash", sizeof(McCoskeyFPSCharacter_eventPlayFlash_Parms), Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Set Damage State" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "SetDamageState", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics
	{
		struct McCoskeyFPSCharacter_eventSetMagicChange_Parms
		{
			float MagicChange;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MagicChange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::NewProp_MagicChange = { "MagicChange", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(McCoskeyFPSCharacter_eventSetMagicChange_Parms, MagicChange), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::NewProp_MagicChange,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Set Damage State" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "SetMagicChange", sizeof(McCoskeyFPSCharacter_eventSetMagicChange_Parms), Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Set Damage State" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "SetMagicState", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Set Magic Value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "SetMagicValue", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics
	{
		struct McCoskeyFPSCharacter_eventUpdateHealth_Parms
		{
			float HealthChange;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HealthChange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::NewProp_HealthChange = { "HealthChange", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(McCoskeyFPSCharacter_eventUpdateHealth_Parms, HealthChange), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::NewProp_HealthChange,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::Function_MetaDataParams[] = {
		{ "Category", "Power" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "void ReceivePointDamage(float Damage, const class UDamageType * DamageType, FVector HitLocation, FVector HitNormal, class UPrimitiveComponent * HitComponent, FName BoneName, FVector ShotFromDirection, class AController * InstigatedBy, AActor * DamageCauser, const FHitResult & HitInfo);" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "UpdateHealth", sizeof(McCoskeyFPSCharacter_eventUpdateHealth_Parms), Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Set Damage State" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMcCoskeyFPSCharacter, nullptr, "UpdateMagic", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMcCoskeyFPSCharacter_NoRegister()
	{
		return AMcCoskeyFPSCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GunOverheatMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GunOverheatMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GunDefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GunDefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MagicCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MagicCurve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_redFlash_MetaData[];
#endif
		static void NewProp_redFlash_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_redFlash;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MagicValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MagicValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousMagic_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreviousMagic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MagicPercentage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MagicPercentage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Magic_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Magic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FullMagic_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FullMagic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealthPercentage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HealthPercentage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Health_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Health;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FullHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FullHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsingMotionControllers_MetaData[];
#endif
		static void NewProp_bUsingMotionControllers_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsingMotionControllers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FireAnimation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FireAnimation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FireSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FireSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectileClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ProjectileClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GunOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GunOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseLookUpRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseLookUpRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_L_MotionController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_L_MotionController;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_R_MotionController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_R_MotionController;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FirstPersonCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FirstPersonCameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VR_MuzzleLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VR_MuzzleLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VR_Gun_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VR_Gun;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FP_MuzzleLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FP_MuzzleLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FP_Gun_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FP_Gun;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mesh1P_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mesh1P;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_McCoskeyFPS,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_DamageTimer, "DamageTimer" }, // 3250557822
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealth, "GetHealth" }, // 628334786
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetHealthIntText, "GetHealthIntText" }, // 278276569
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagic, "GetMagic" }, // 169833187
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_GetMagicIntText, "GetMagicIntText" }, // 3323556667
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_PlayFlash, "PlayFlash" }, // 3420603799
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetDamageState, "SetDamageState" }, // 3719133623
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicChange, "SetMagicChange" }, // 1878386376
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicState, "SetMagicState" }, // 1474664818
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_SetMagicValue, "SetMagicValue" }, // 2117871460
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateHealth, "UpdateHealth" }, // 3938212726
		{ &Z_Construct_UFunction_AMcCoskeyFPSCharacter_UpdateMagic, "UpdateMagic" }, // 2472947146
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "McCoskeyFPSCharacter.h" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOverheatMaterial_MetaData[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOverheatMaterial = { "GunOverheatMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, GunOverheatMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOverheatMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOverheatMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunDefaultMaterial_MetaData[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunDefaultMaterial = { "GunDefaultMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, GunDefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunDefaultMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunDefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicCurve_MetaData[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicCurve = { "MagicCurve", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, MagicCurve), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicCurve_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicCurve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_redFlash_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_redFlash_SetBit(void* Obj)
	{
		((AMcCoskeyFPSCharacter*)Obj)->redFlash = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_redFlash = { "redFlash", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMcCoskeyFPSCharacter), &Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_redFlash_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_redFlash_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_redFlash_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicValue_MetaData[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicValue = { "MagicValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, MagicValue), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_PreviousMagic_MetaData[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_PreviousMagic = { "PreviousMagic", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, PreviousMagic), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_PreviousMagic_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_PreviousMagic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicPercentage_MetaData[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicPercentage = { "MagicPercentage", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, MagicPercentage), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicPercentage_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicPercentage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Magic_MetaData[] = {
		{ "Category", "Magic" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Magic = { "Magic", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, Magic), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Magic_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Magic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullMagic_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullMagic = { "FullMagic", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, FullMagic), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullMagic_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullMagic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_HealthPercentage_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_HealthPercentage = { "HealthPercentage", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, HealthPercentage), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_HealthPercentage_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_HealthPercentage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Health_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Health = { "Health", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, Health), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Health_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Health_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullHealth_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullHealth = { "FullHealth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, FullHealth), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_bUsingMotionControllers_MetaData[] = {
		{ "Category", "Gameplay" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Whether to use motion controller location for aiming." },
	};
#endif
	void Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_bUsingMotionControllers_SetBit(void* Obj)
	{
		((AMcCoskeyFPSCharacter*)Obj)->bUsingMotionControllers = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_bUsingMotionControllers = { "bUsingMotionControllers", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(AMcCoskeyFPSCharacter), &Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_bUsingMotionControllers_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_bUsingMotionControllers_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_bUsingMotionControllers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireAnimation_MetaData[] = {
		{ "Category", "Gameplay" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "AnimMontage to play each time we fire" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireAnimation = { "FireAnimation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, FireAnimation), Z_Construct_UClass_UAnimMontage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireAnimation_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireAnimation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireSound_MetaData[] = {
		{ "Category", "Gameplay" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Sound to play each time we fire" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireSound = { "FireSound", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, FireSound), Z_Construct_UClass_USoundBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_ProjectileClass_MetaData[] = {
		{ "Category", "Projectile" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Projectile class to spawn" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_ProjectileClass = { "ProjectileClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, ProjectileClass), Z_Construct_UClass_AMcCoskeyFPSProjectile_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_ProjectileClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_ProjectileClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOffset_MetaData[] = {
		{ "Category", "Gameplay" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Gun muzzle's offset from the characters location" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOffset = { "GunOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, GunOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseLookUpRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Base look up/down rate, in deg/sec. Other scaling may affect final rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseLookUpRate = { "BaseLookUpRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, BaseLookUpRate), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseLookUpRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseLookUpRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseTurnRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseTurnRate = { "BaseTurnRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, BaseTurnRate), METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseTurnRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_L_MotionController_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "McCoskeyFPSCharacter" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Motion controller (left hand)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_L_MotionController = { "L_MotionController", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, L_MotionController), Z_Construct_UClass_UMotionControllerComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_L_MotionController_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_L_MotionController_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_R_MotionController_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "McCoskeyFPSCharacter" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Motion controller (right hand)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_R_MotionController = { "R_MotionController", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, R_MotionController), Z_Construct_UClass_UMotionControllerComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_R_MotionController_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_R_MotionController_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FirstPersonCameraComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "First person camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FirstPersonCameraComponent = { "FirstPersonCameraComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, FirstPersonCameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FirstPersonCameraComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FirstPersonCameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_MuzzleLocation_MetaData[] = {
		{ "Category", "Mesh" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Location on VR gun mesh where projectiles should spawn." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_MuzzleLocation = { "VR_MuzzleLocation", nullptr, (EPropertyFlags)0x00400000000b0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, VR_MuzzleLocation), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_MuzzleLocation_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_MuzzleLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_Gun_MetaData[] = {
		{ "Category", "Mesh" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_Gun = { "VR_Gun", nullptr, (EPropertyFlags)0x00400000000b0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, VR_Gun), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_Gun_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_Gun_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_MuzzleLocation_MetaData[] = {
		{ "Category", "Mesh" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Location on gun mesh where projectiles should spawn." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_MuzzleLocation = { "FP_MuzzleLocation", nullptr, (EPropertyFlags)0x00400000000b0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, FP_MuzzleLocation), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_MuzzleLocation_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_MuzzleLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_Gun_MetaData[] = {
		{ "Category", "Mesh" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Gun mesh: 1st person view (seen only by self)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_Gun = { "FP_Gun", nullptr, (EPropertyFlags)0x00400000000b0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, FP_Gun), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_Gun_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_Gun_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Mesh1P_MetaData[] = {
		{ "Category", "Mesh" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "McCoskeyFPSCharacter.h" },
		{ "ToolTip", "Pawn mesh: 1st person view (arms; seen only by self)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Mesh1P = { "Mesh1P", nullptr, (EPropertyFlags)0x00400000000b0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMcCoskeyFPSCharacter, Mesh1P), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Mesh1P_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Mesh1P_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOverheatMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunDefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_redFlash,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_PreviousMagic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_MagicPercentage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Magic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullMagic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_HealthPercentage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Health,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FullHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_bUsingMotionControllers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireAnimation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FireSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_ProjectileClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_GunOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseLookUpRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_BaseTurnRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_L_MotionController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_R_MotionController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FirstPersonCameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_MuzzleLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_VR_Gun,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_MuzzleLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_FP_Gun,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::NewProp_Mesh1P,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMcCoskeyFPSCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::ClassParams = {
		&AMcCoskeyFPSCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMcCoskeyFPSCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMcCoskeyFPSCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMcCoskeyFPSCharacter, 3539712843);
	template<> MCCOSKEYFPS_API UClass* StaticClass<AMcCoskeyFPSCharacter>()
	{
		return AMcCoskeyFPSCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMcCoskeyFPSCharacter(Z_Construct_UClass_AMcCoskeyFPSCharacter, &AMcCoskeyFPSCharacter::StaticClass, TEXT("/Script/McCoskeyFPS"), TEXT("AMcCoskeyFPSCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMcCoskeyFPSCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
