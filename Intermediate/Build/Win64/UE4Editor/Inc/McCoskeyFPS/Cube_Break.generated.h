// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef MCCOSKEYFPS_Cube_Break_generated_h
#error "Cube_Break.generated.h already included, missing '#pragma once' in Cube_Break.h"
#endif
#define MCCOSKEYFPS_Cube_Break_generated_h

#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCompHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCompHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCompHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCompHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACube_Break(); \
	friend struct Z_Construct_UClass_ACube_Break_Statics; \
public: \
	DECLARE_CLASS(ACube_Break, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(ACube_Break)


#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACube_Break(); \
	friend struct Z_Construct_UClass_ACube_Break_Statics; \
public: \
	DECLARE_CLASS(ACube_Break, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/McCoskeyFPS"), NO_API) \
	DECLARE_SERIALIZER(ACube_Break)


#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACube_Break(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACube_Break) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACube_Break); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACube_Break); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACube_Break(ACube_Break&&); \
	NO_API ACube_Break(const ACube_Break&); \
public:


#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACube_Break(ACube_Break&&); \
	NO_API ACube_Break(const ACube_Break&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACube_Break); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACube_Break); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACube_Break)


#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_PRIVATE_PROPERTY_OFFSET
#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_11_PROLOG
#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_RPC_WRAPPERS \
	artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_INCLASS \
	artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_PRIVATE_PROPERTY_OFFSET \
	artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_INCLASS_NO_PURE_DECLS \
	artifact_3_Source_McCoskeyFPS_Cube_Break_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MCCOSKEYFPS_API UClass* StaticClass<class ACube_Break>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact_3_Source_McCoskeyFPS_Cube_Break_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
