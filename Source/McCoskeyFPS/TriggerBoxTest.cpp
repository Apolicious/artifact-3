

// Fill out your copyright notice in the Description page of Project Settings.

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,text)
#define printFString(text, fstring) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(text), fstring))

#include "TriggerBoxTest.h"
// include draw debug helpers header file
#include "Components/PointLightComponent.h"
#include "Components/SkyLightComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
ATriggerBoxTest::ATriggerBoxTest()
{
	PrimaryActorTick.bCanEverTick = true;

	LightIntensity = 1.0f;

	PointLight = CreateDefaultSubobject<USkyLightComponent>(TEXT("Point Light"));
	PointLight->Intensity = LightIntensity;
	PointLight->bVisible = true;
	RootComponent = PointLight;

	
	//Register Events
	OnActorBeginOverlap.AddDynamic(this, &ATriggerBoxTest::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &ATriggerBoxTest::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ATriggerBoxTest::BeginPlay()
{
	Super::BeginPlay();

	DrawDebugBox(GetWorld(), GetActorLocation(), GetComponentsBoundingBox().GetExtent(), FColor::Green, true, -1, 0, 5);

}

void ATriggerBoxTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATriggerBoxTest::ToggleLight()
{
	PointLight->ToggleVisibility();
}

void ATriggerBoxTest::OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor)
{
	//if the overlapping actor is the specific actor we identified in the editor
	if (OtherActor && (OtherActor != this) && OtherActor == SpecificActor)
	{
		ToggleLight();
	}
}

void ATriggerBoxTest::OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor)
{
	//if the overlapping actor is the specific actor we identified in the editor
	if (OtherActor && (OtherActor != this) && OtherActor == SpecificActor)
	{
		ToggleLight();
	}
}