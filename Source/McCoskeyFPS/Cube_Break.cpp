// Fill out your copyright notice in the Description page of Project Settings.


#include "Cube_Break.h"
// include files
#include "Components/BoxComponent.h"
// Sets default values
#include "Kismet/GameplayStatics.h"
#include "McCoskeyFPSProjectile.h"

ACube_Break::ACube_Break()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation
	MyComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	MyComp->SetSimulatePhysics(true);
	MyComp->SetNotifyRigidBodyCollision(true);

	MyComp->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	MyComp->OnComponentHit.AddDynamic(this, &ACube_Break::OnCompHit);

	// Set as root component
	RootComponent = MyComp;
}

// Called when the game starts or when spawned
void ACube_Break::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACube_Break::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACube_Break::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor  && (OtherActor != this) && (OtherActor == Cast<AMcCoskeyFPSProjectile>(OtherActor)))
	{
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *OtherActor->GetName()));
	}
}
