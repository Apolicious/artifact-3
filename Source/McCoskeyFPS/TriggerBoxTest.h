// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Engine.h"
#include "TriggerBoxTest.generated.h"

/**
 * 
 */
UCLASS()
class MCCOSKEYFPS_API ATriggerBoxTest : public ATriggerBox
{
	GENERATED_BODY()
	
protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	
public:

	// constructor sets default values for this actor's properties
	ATriggerBoxTest();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// overlap begin function
	UFUNCTION()
		void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	// overlap end function
	UFUNCTION()
		void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);

	// specific actor for overlap
	UPROPERTY(EditAnywhere)
		class AActor* SpecificActor;

	// declare point light comp
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
		class USkyLightComponent* PointLight;

	// declare light intensity variable
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
		float LightIntensity;

	// declare ToggleLight function
	UFUNCTION()
		void ToggleLight();

};
