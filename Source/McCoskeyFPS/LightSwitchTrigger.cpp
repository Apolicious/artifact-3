// Fill out your copyright notice in the Description page of Project Settings.
//FOR CLASS!!!!!
//the C++ code OnOverlapBegin checks if another components vector (3D) location resides withing its own 3D parameter space
//this overlap creates a boolean that becomes true once that happens.  that boolean becomes a trigger for any effect
//that the programmer can code, such as applying damage, flipping a switch, changing behavior etc....

#include "LightSwitchTrigger.h"
#include "Components/PointLightComponent.h"
#include "Components/SkyLightComponent.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
// include draw debu helpers header file
#include "DrawDebugHelpers.h"

// Sets default values
ALightSwitchTrigger::ALightSwitchTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LightIntensity = 3000.0f;

	PointLight = CreateDefaultSubobject<USkyLightComponent>(TEXT("Point Light"));
	PointLight->Intensity = LightIntensity;
	PointLight->bVisible = true;
	RootComponent = PointLight;

	LightSphere = CreateDefaultSubobject<UBoxComponent>(TEXT("Light Sphere Component"));
	//LightSphere->InitSphereRadius(300.0f);
	LightSphere->SetCollisionProfileName(TEXT("Trigger"));
	LightSphere->SetupAttachment(RootComponent);

	OnActorBeginOverlap.AddDynamic(this, &ALightSwitchTrigger::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &ALightSwitchTrigger::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ALightSwitchTrigger::BeginPlay()
{
	Super::BeginPlay();
	
	DrawDebugSphere(GetWorld(), GetActorLocation(), 300.f, 50, FColor::Green, true, -1, 0, 2);
}

// Called every frame
void ALightSwitchTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALightSwitchTrigger::ToggleLight()
{
	PointLight->ToggleVisibility();
}

void ALightSwitchTrigger::OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor)
{
	if (OtherActor && (OtherActor != this) && OtherActor == SpecificActor)
	{
		ToggleLight();
	}
}

void ALightSwitchTrigger::OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor)
{
	if (OtherActor && (OtherActor != this) && OtherActor == SpecificActor)
	{
		ToggleLight();
	}
}