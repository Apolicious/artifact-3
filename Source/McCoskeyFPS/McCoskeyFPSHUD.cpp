// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
// FOR CLASS!!!
// I have created 3 HUDs for this game. One for the health, one to keep track of score, and one that pauses and shows up
// once the game is finished.  The health ui is updated everytime the player and escorted muffin takes damage.  Objects in the world can
// damage both the player and escorted muffin.  The enemy muffins can also damage either.  The health UI will also update if 
// health is added to either.  the 2nd UI (which can be added to the first) just keeps track of the game score.  Every time
// you kill and enemy muffin, you will add one to the score.  Once either you or the escorted muffin dies, you the game will end
// and your score total will be shown on the 3rd HUD.  This one also pauses the game and give the option of restarting the level


#include "McCoskeyFPSHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"

AMcCoskeyFPSHUD::AMcCoskeyFPSHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;

	static ConstructorHelpers::FClassFinder<UUserWidget> HealthBarObj(TEXT("/Game/FirstPerson/UI/Health_UI"));
	HUDWidgetClass = HealthBarObj.Class;
}


void AMcCoskeyFPSHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

void AMcCoskeyFPSHUD::BeginPlay()
{
	Super::BeginPlay();

	if (HUDWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);

		if (CurrentWidget)
		{
			CurrentWidget->AddToViewport();
		}
	}
}
