// Fill out your copyright notice in the Description page of Project Settings.
//FOR CLASS!!!
//Each camera can display either an orthographic or perspective feild of view.  the perspective FoV creates a simulated 3-dimensional
//view where objects closer are larger and objects further away are smaller.  the Orthographic view creates a simulated
//view where everthing looks to be the same size and distance from the camera, effectively creating a 2-Dimensional viewing area
//

#include "Camera_Director.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACamera_Director::ACamera_Director()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACamera_Director::BeginPlay()
{
	Super::BeginPlay();
	

		// Find the actor that handles control for the local player.
		APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
		if (OurPlayerController)
		{
			if ((OurPlayerController->GetViewTarget() != CameraOne) && (CameraOne != nullptr))
			{
				// Cut instantly to camera one.
				OurPlayerController->SetViewTarget(CameraOne);
			}
			
		}
	
}

// Called every frame
void ACamera_Director::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	const float TimeBetweenCameraChanges = 2.0f;
	const float SmoothBlendTime = 2.0f;
	const float WaitTime = 0.0f;
	TimeToNextCameraChange -= DeltaTime;
	if (TimeToNextCameraChange <= 0.0f)
	{
		TimeToNextCameraChange += TimeBetweenCameraChanges;

		// Find the actor that handles control for the local player.
		APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
		if (OurPlayerController)
		{
			if ((OurPlayerController->GetViewTarget() != CameraTwo) && (CameraTwo != nullptr))
			{
				// Blend smoothly to camera two.
				OurPlayerController->SetViewTargetWithBlend(CameraTwo, SmoothBlendTime);
				
			}
		}
	}

}

