// Fill out your copyright notice in the Description page of Project Settings.
//FOR CLASS!!!
//AI controllers use built in navigation settings from an in scene NAVMESH.  The navmesh already contains the algorithms
//and settings for the movement of objects.  It gets put on the game scene and updates and creates areas where the controller
//can move.  This allows for 'intelligent' movement and behavior trees to exist, instead of using physics to force move
//vectors.

#include "TagController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

void ATagController::BeginPlay()
{
	Super::BeginPlay();
	//this code "grabs" all the targetpoints in a level, and sets them as waypoints
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints);

	GoToRandomWaypoint();
}

//this selects a waypoint at random
ATargetPoint* ATagController::GetRandomWaypoint()
{
	auto index = FMath::RandRange(0, Waypoints.Num() - 1);
	return Cast<ATargetPoint>(Waypoints[index]);
}

//this tells the controller to move to the random waypoint selected using the navmesh within the level.
void ATagController::GoToRandomWaypoint()
{
	MoveToActor(GetRandomWaypoint());
}

//tells the controller to wait 1.0 seconds and look for a new random waypoint.
void ATagController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	GetWorldTimerManager().SetTimer(TimerHandle, this, &ATagController::GoToRandomWaypoint, 1.0f, false);
}